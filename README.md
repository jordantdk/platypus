#Platypus .pdf

Runs in the terminal, platypus .pdf helps you manage your pdf with ease. It can fetch pdfs from the web for you, open pdfs from a specified path etc.

 - This project uses the Google Search API to download pdfs from the web.
 
 
#Current features include:
- Download pdf from the web
- Download website as pdf
- Open pdf
- View recent files
 
 
#Features to add:
- Backup pdf folder
- Send pdf to any device on LAN
