#!/usr/bin/env python3

import pdfkit
import os
from tqdm import tqdm
from clmenu import Menu, printLogo,getch
import re
import yaml
import requests

'''
TO DO LIST:
	ADD FEATURE: SEND FILE OVER SOCKET TO OTHER DEVICE ON LAN
	ADD FEATURE: EMAIL PDF TO YOURSELF
	ADD FEATURE: BACKUP PDF FOLDER

	Modules:
		sockets
'''

mygetch=getch()
maindir=os.getcwd() #working directory of program

baseurl="https://www.googleapis.com/customsearch/v1?key={}&cx={}&q={}&fileType=pdf"
apikey=''
cx=''
pdflibrary=''

'''
copy function that copies a pdf to a specified location
delete function to delete specified pdf
backup function that uploads or updates a folder in the cloud with added pdfs
'''
def nonalpha2(string,spaces=False):
	'''
		constructs a regex expression for
		non-alphanumeric characters in a string
	'''
	nons=re.findall("(\W)+",string)#finds all non alphanumeric characters
	#print("matches: "+str(nons))
	for i in range(len(nons)):
		if i!=0:
			sublist=nons[0:i]
			if nons[i] in sublist:
				continue
		if(spaces):
			base="\{}".format(nons[i])
		else:
			base="(\{})".format(nons[i])
		#replaces the non alphanumeric character with base (regex)
		string = re.sub(r"\{}".format(nons[i]),base,string)
	return string

def downloadPDF():
	'''
		Download pdf from the web
		Download website as pdf
	'''
	options=["REGULAR PDF","WEBSITE AS PDF ","GO BACK  "]
	instructions="\n\t\t\t\tSELECT THE TYPE OF PDF YOU WANT TO DOWNLOAD"
	menu=Menu(options,instructions,"pdflogo.txt")
	temp=9
	while(temp!=2):
		temp=menu.prompt()
		if(temp==0):
			searchweb()
		elif temp==1:
			webpdf()

class PDFsource:

	def __init__(self,title,url):
		self.title=title
		self.url=url
		self.filename=url.split('/')[-1]
		self.display_text=title+" : "+url.split('/')[-1]

	def display(self):
		print(self.display_text)
		
	def download(self):
		printLogo("pdflogo.txt")
		os.chdir(pdflibrary)
		self.filename=input("\t\t\tENTER WHAT YOU WANT THE FILENAME OF THE PDF TO BE: ")
		self.filename=nonalpha2(self.filename,True)
		try:
			response=requests.get(self.url,stream=True)
			total_size=int(response.headers['content-length'])
			with open(self.filename,"wb") as file:
				for data in tqdm(iterable=response.iter_content(chunk_size=128),total=total_size/128,unit='KB'):
					file.write(data)
			print("\n\t\t\tDOWNLOAD COMPLETE!")
			os.system("evince "+self.filename)
		except:
			print("\n\t\t\t\t\tFILE COULD NOT BE DOWNLOADED")
		finally:
			mygetch()
			os.chdir(maindir)

def saveAPIkey(pairs):
	with open("pdf-config.yml",'r') as file:
		data=yaml.load(file)
		data['searchAPIkey']=pairs['key']
		data['cx']=pairs['engine']
	with open("pdf-config.yml",'w') as file:
		yaml.dump(data,file)
	global apikey
	global cx
	apikey=pairs['key']
	cx=pairs['engine']
		
def getAPIkey():
	printLogo("pdflogo.txt")
	while True:
		opts=["YES","NO"]
		global apikey
		if(apikey==''):
			instructions="\t DO YOU HAVE A GOOGLE API KEY?"
			menu=Menu(opts,instructions,"pdflogo.txt")
			temp=menu.prompt()
			if(opts[temp]=="YES"):
				apikey=input("\t\t\t\t   ENTER THE API KEY: ")
			else:
				return "NULL"
		global cx
		if(cx==''):
			instructions="\t DO YOU HAVE A SEARCH ENGINE ID?"
			menu=Menu(opts,instructions,"pdflogo.txt")
			temp=menu.prompt()
			if(opts[temp]=="YES"):
				cx=input("\t\t\t\t   ENTER THE SEARCH ENGINE ID: ")
			else:
				return "NULL"
		q='testing'
		response=requests.get(baseurl.format(apikey,cx,q))
		response=response.json()
		if('error' not in response.keys()):
			pair={'key':apikey,'engine':cx}
			saveAPIkey(pair)
			return pair
		else:
			print("\n\t\t\t\t    INVALID API CREDENTIALS")
			mygetch()


def searchweb():
	printLogo("pdflogo.txt")
	pair=getAPIkey()
	if(pair!="NULL"):
		query=input("\n\n\t\t\t\t\t\tSEARCH GOOGLE: ")
		try:
			response=requests.get(baseurl.format(apikey,cx,query))
			data=response.json()
			print(data)
			results=data['items']
			sources=[]#list for storing each source from response
			options=[]#menu options
			instructions="\t  SELECT THE SOURCE TO DOWNLOAD THE CORRESPONDING PDF"
			for item in results:
				title=item['title']
				url=item['link']
				src=PDFsource(title,url)
				sources.append(src)
				options.append(src.display_text)
			options.append("GO BACK")
			menu=Menu(options,instructions,"pdflogo.txt")
			temp=menu.prompt(False)
			if(temp!=10):
				src=sources[temp]
				src.download()
		except Exception:
			print("\t\t\t\t\tERROR : CANNOT ACCESS THE WEB FOR AN UNKNOWN REASON.")
			mygetch()

def getlibpath():
	printLogo("pdflogo.txt")
	path=" "
	while(not os.path.exists(path)):
		path=input("\n\t\t\t\t\t\tENTER A VALID PATH TO YOUR PDF LIBRARY: ")
	#get settings
	with open("pdf-config.yml","r") as file:
		data=yaml.load(file)
	data['LIBRARY']=path
	global pdflibrary
	pdflibrary=path
	with open("pdf-config.yml","w") as file:
		yaml.dump(data,file)#overwrite old settings
	print("\n\n\t\t\t\t\t\tYOUR NEW PATH HAS BEEN SAVED.")
	mygetch()

def initialize():
	if(not os.path.exists("pdf-config.yml")):
		getlibpath()#prompts for path to pdf directory
		initializePath()
	else:
		with open("pdf-config.yml","r") as file:
			data=yaml.load(file)
			global pdflibrary
			global apikey
			global cx
			pdflibrary=data['LIBRARY']
			apikey=data['searchAPIkey']
			cx=data['cx']

initialize()

def openpdf():
	options=["SEARCH    ","RECENT FILES ","GO BACK    "]
	instructions="\t     OPEN PDF\n\n\t\t\t\t\t  SELECT AN OPTION"
	menu=Menu(options,instructions,"pdflogo.txt")
	temp=9
	while(temp!=2):
		temp=menu.prompt()
		if(temp==0):
			pdf=searchlocalpdf()
			if("GO BACK" not in pdf):
				if(" " in pdf):
					pdf=nonalpha2(pdf,True)#puts a '\' before space
				os.chdir(pdflibrary)
				#os.system("evince "+pdf) optional
				os.system("xdg-open "+pdf)
				os.chdir(maindir)
		elif(temp==1):
			os.system("evince")

def webpdf():
	printLogo("pdflogo.txt")
	website=input("\n\t\t\tENTER THE URL FOR THE WEBSITE: ")
	name=input("\n\t\t\tENTER WHAT YOU WANT THE PDF TO BE NAMED: ")
	try:
		os.chdir(pdflibrary)
		pdfkit.from_url(website,name)
		print("\n\t\t\t\tPDF CREATION SUCCESSFUL.")
		os.system("evince "+nonalpha2(name,True))
	except:
		print("\n\n\t\t\t\t\tERROR WHEN ATTEMPTING PDF CREATION")
	finally:
		mygetch()
		os.chdir(maindir)

def searchlocalpdf():
	printLogo("pdflogo.txt")
	os.chdir(pdflibrary)
	pdfs=os.listdir()
	#print(pdfs)
	q=input("\n\n\t\t\t\t\t\tSEARCH FOR PDF: ")
	base_regex=r".*(?i){}(?i).*"#matches capital and common
	sub_regex=nonalpha2(q)
	regex=base_regex.format(sub_regex)
	r=re.compile(base_regex.format(sub_regex))
	matches=list(filter(r.match,pdfs))
	#print(str(matches))
	if(len(matches)==0):
		instructions="\n\t\t\t\t\t    NO PDFs WERE FOUND"

	else:
		instructions="\t\tSELECT THE PDF YOU WANT TO OPEN"
	matches.append("GO BACK")
	menu=Menu(matches,instructions,"pdflogo.txt")
	os.chdir(maindir)#go back to main directory for printing header
	indicator=menu.prompt()
	return matches[indicator]

def main():
	directory=os.getcwd()
	options=["DOWNLOAD PDF","OPEN PDF  ","RECENT PDF ","EDIT LIBRARY PATH ","EXIT    "]
	instructions="\n\t\t\t\t\t    SELECT AN OPTION\n\n\t\t\t\t\tUSE ARROW KEYS TO NAVIGATE"
	menu=Menu(options,instructions,"pdflogo.txt")
	temp=9
	while(temp!=len(options)-1):
		os.chdir(directory)
		temp=menu.prompt()
		if(temp==0):
			downloadPDF()
		elif(temp==1):
			openpdf()
		elif(temp==2):
			os.system("evince")
		elif(temp==3):
			getlibpath()
	printLogo("pdflogo.txt")
	print("Goodbye")

if __name__=="__main__":
	main()
